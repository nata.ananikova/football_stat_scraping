import glob
import time as time
from selenium import webdriver
from selenium.webdriver.common.by import By
import pandas as pd
import os
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
import re


# def scroll_and_save_table_content():
#     f = driver.find_element(By.CSS_SELECTOR, "#wrap #content #all_team_stats #div_team_stats #team_stats")
#     driver.execute_script("arguments[0].scrollIntoView(true);", f)
#     f_text = f.text
#     return f_text
#
#
# def scroll_and_save_final_points():
#     points = driver.find_element(By.CSS_SELECTOR, "#wrap #content .linescore_wrap .linescore")
#     driver.execute_script("arguments[0].scrollIntoView(true);", points)
#     rows = points.find_elements(By.TAG_NAME, "tr")
#     rows_away = rows[1].text.split('\n')
#     rows_home = rows[2].text.split('\n')
#     global away_points
#     global home_points
#     global Home_Win
#     away_points = [el.split(' ') for el in rows_away][0][-1]
#     home_points = [el.split(' ') for el in rows_home][0][-1]
#     Home_Win = True if int(home_points) > int(away_points) else False
#     return away_points, home_points, Home_Win
#
#
# def split_list(lst):
#     res = [el.split(' ') for el in lst]
#     return res
#
#
# def table_content_to_df(name_txt, finish_df):
#     lines_lst = []
#     with open(name_txt, 'r') as f:
#         alist = [line.rstrip() for line in f]
#         lines_lst.append(alist)
#
#     for i in lines_lst:
#         df = pd.DataFrame({
#             'NAME_AWAY': split_list(i)[0][1].split('-')[0],
#             'away_firstdowns': [int(split_list(i)[1][2].split('-')[0])],
#             'away_rush': [int(split_list(i)[2][1].split('-')[0])],
#             'away_rush_yards': [int(split_list(i)[2][1].split('-')[1])],
#             'away_rushtds': [int(split_list(i)[2][1].split('-')[2])],
#             'away_competions': [int(split_list(i)[3][1].split('-')[0])],
#             'away_pass_att': [int(split_list(i)[3][1].split('-')[1])],
#             'away_pass_yards': [int(split_list(i)[3][1].split('-')[2])],
#             'away_pass_td': [int(split_list(i)[3][1].split('-')[3])],
#             'away_pass_int': [int(split_list(i)[3][1].split('-')[4])],
#             'away_total_yards': [int(split_list(i)[4][2].split('-')[0])],
#             'away_fumbles': [int(split_list(i)[5][1].split('-')[0])],
#             'away_fumbles_lost': [int(split_list(i)[5][1].split('-')[1])],
#             'away_turnovers': [int(split_list(i)[6][1].split('-')[0])],
#             'away_penalities': [int(split_list(i)[7][1].split('-')[0])],
#             'away_penality_yards': [int(split_list(i)[7][1].split('-')[1])],
#             'away_points': away_points,
#             'NAME_HOME': split_list(i)[0][2].split('-')[0],
#             'home_firstdowns': [int(split_list(i)[1][3].split('-')[0])],
#             'home_rush': [int(split_list(i)[2][2].split('-')[0])],
#             'home_rush_yards': [int(split_list(i)[2][2].split('-')[1])],
#             'home_rushtds': [int(split_list(i)[2][2].split('-')[2])],
#             'home_competions': [int(split_list(i)[3][2].split('-')[0])],
#             'home_pass_att': [int(split_list(i)[3][2].split('-')[1])],
#             'home_pass_yards': [int(split_list(i)[3][2].split('-')[2])],
#             'home_pass_td': [int(split_list(i)[3][2].split('-')[3])],
#             'home_pass_int': [int(split_list(i)[3][2].split('-')[4])],
#             'home_total_yards': [int(split_list(i)[4][3].split('-')[0])],
#             'home_fumbles': [int(split_list(i)[5][2].split('-')[0])],
#             'home_fumbles_lost': [int(split_list(i)[5][2].split('-')[1])],
#             'home_turnovers': [int(split_list(i)[6][2].split('-')[0])],
#             'home_penalities': [int(split_list(i)[7][2].split('-')[0])],
#             'home_penality_yards': [int(split_list(i)[7][2].split('-')[1])],
#             'home_points': home_points,
#             'Home_Win': Home_Win
#         })
#         df.to_csv(finish_df)
#         return df


if __name__ == "__main__":
    # driver = webdriver.Firefox()
    # driver.install_addon('adblockultimate@adblockultimate.net.xpi', temporary=True)
    # # driver.get(" ")
    # time.sleep(1)
    # # Accept cookies
    # driver.find_element(By.CSS_SELECTOR, "#qc-cmp2-ui .qc-cmp2-footer .css-47sehv").click()
    #
    # while True:
    #     name_new_dir = f'{driver.find_element(By.CSS_SELECTOR, "#wrap #content").find_element(By.XPATH, ".//h1").text[35:]}'
    #     os.mkdir(os.path.join('.', name_new_dir))
    #
    #     # How many game boxes on Monday on page
    #     try:
    #         game_monday_card_quantity = driver.find_element(By.CSS_SELECTOR, "#wrap #content").find_element(By.XPATH,
    #                                                                                              './/div[@class="game_summaries"][h2[contains(text(), "Monday, January 1, 2018")]]')
    #
    #         num_monday_last_game_box = int(((len(game_monday_card_quantity.text.split('\n')) - 1) / 3)+1)
    #
    #         for i in range(0, num_monday_last_game_box):
    #             main_block_element = driver.find_element(By.CSS_SELECTOR, "#wrap #content").find_element(By.XPATH,
    #                                                                                                      f'.//div[@class="game_summaries"][h2[contains(text(), "Monday, January 1, 2018")]]/div[@class="game_summary nohover "][{i}]/table['
    #                                                                                                      f'@class="teams"]/tbody/tr['
    #                                                                                                      f'2]/td[@class="right gamelink"]/a')
    #
    #             if i <= 16:
    #                 ActionChains(driver).move_to_element(main_block_element).click(main_block_element).perform()
    #
    #             time.sleep(1)
    #             scroll_and_save_final_points()
    #
    #             with open(f'{name_new_dir}/{i}.txt', 'w') as f:
    #                 f.write(scroll_and_save_table_content())
    #
    #             try:
    #                 table_content_to_df(f'{name_new_dir}/{i}.txt', f'{name_new_dir}/{i}.csv')
    #             except:
    #                 print(f"{i}: An exception occurred")
    #                 pass
    #             driver.back()
    #         driver.execute_script("window.scrollTo(0, -1600)")
    #         next_week_button = driver.find_element(By.CSS_SELECTOR, "#wrap #content").find_element(By.LINK_TEXT,
    #                                                                                                    "Next Week")
    #         time.sleep(2)
    #         next_week_button.click()
    #     except NoSuchElementException:
    #         pass
    #
        # # How many game boxes in the section "Other Games This Week"
        # try:
        #     game_card_quantity = driver.find_element(By.CSS_SELECTOR, "#wrap #content").find_element(By.XPATH,
        #                                                                                          './/div[@class="game_summaries"][h2[contains(text(), "Other Games This Week")]]')
        #
        #     num_last_game_box = int(((len(game_card_quantity.text.split('\n')) - 1) / 3)+1)
        #
        #     for i in range(1, num_last_game_box):
        #         main_block_element = driver.find_element(By.CSS_SELECTOR, "#wrap #content").find_element(By.XPATH,
        #                                                                                              f'.//div[@class="game_summaries"][h2[contains(text(), "Other Games '
        #                                                                                              f'This Week")]]/div[@class="game_summary nohover "][{i}]/table['
        #                                                                                              f'@class="teams"]/tbody/tr['
        #                                                                                              f'2]/td[@class="right gamelink"]/a')
    #
    #             if i <= 16:
    #                 ActionChains(driver).move_to_element(main_block_element).click(main_block_element).perform()
    #             elif 16 < i <= 48:
    #                 driver.execute_script("window.scrollTo(0, 650)")
    #                 time.sleep(1)
    #                 ActionChains(driver).move_to_element(main_block_element).click(main_block_element).perform()
    #             elif 48 < i <= 80:
    #                 driver.execute_script("window.scrollTo(0, 1300)")
    #                 time.sleep(1)
    #                 ActionChains(driver).move_to_element(main_block_element).click(main_block_element).perform()
    #             else:
    #                 driver.execute_script("window.scrollTo(0, 1550)")
    #                 time.sleep(1)
    #                 ActionChains(driver).move_to_element(main_block_element).click(main_block_element).perform()
    #
    #                 #     driver.execute_script("window.scrollTo(0, 100)")
    #                 #     driver.execute_script("arguments[0].scrollIntoView();", main_block_element)
    #                 #     driver.execute_script("arguments[0].click();", main_block_element)
    #
    #             time.sleep(1)
    #             scroll_and_save_final_points()
    #
    #             with open(f'{name_new_dir}/{i}.txt', 'w') as f:
    #                 f.write(scroll_and_save_table_content())
    #
    #             try:
    #                 table_content_to_df(f'{name_new_dir}/{i}.txt', f'{name_new_dir}/{i}.csv')
    #             except:
    #                 print(f"{i}: An exception occurred")
    #                 pass
    #             driver.back()
    #         driver.execute_script("window.scrollTo(0, -1600)")
    #         next_week_button = driver.find_element(By.CSS_SELECTOR, "#wrap #content").find_element(By.LINK_TEXT,
    #                                                                                                "Next Week")
    #         time.sleep(2)
    #         next_week_button.click()
    #     except NoSuchElementException:
    #         pass

    # generated directories
    train_dir_lst = ['September 25, 2017', 'September 9, 2019', 'September 18, 2017', 'October 8, 2018',
                     'January 1, 2018', 'December 2, 2019', 'August 28, 2017', 'November 5, 2018',
                     'September 23, 2019', 'January 7, 2019', 'December 24, 2018', 'October 28, 2019',
                     'November 12, 2018', 'December 4, 2017', 'October 15, 2018', 'October 9, 2017',
                     'November 6, 2017', 'September 24, 2018', 'December 23, 2019', 'December 3, 2018',
                     'November 4, 2019','October 16, 2017', 'January 6, 2020', 'December 18, 2017', 'November 13, 2017',
                     'October 29, 2018','December 25, 2017', 'November 11, 2019', 'October 30, 2017', 'October 14, 2019',
                     'January 8, 2018', 'November 20, 2017', 'December 16, 2019', 'September 2, 2019', 'September 11, 2017',
                     'October 1, 2018', 'December 9, 2019', 'December 30, 2019', 'September 17, 2018', 'November 19, 2018',
                     'October 23, 2017', 'October 7, 2019', 'September 4, 2017', 'November 26, 2018', 'October 21, 2019',
                     'December 10, 2018', 'August 27, 2018', 'December 17, 2018', 'September 3, 2018', 'October 2, 2017',
                     'September 10, 2018', 'December 31, 2018', 'September 16, 2019', 'November 18, 2019', 'November 25, 2019',
                     'October 22, 2018',  'September 30, 2019', 'September 7, 2020', 'December 11, 2017','November 27, 2017',
                     'August 26, 2019', 'November 30, 2020', 'January 13, 2020', 'November 16, 2020', 'September 21, 2020',
                     'December 21, 2020', 'October 12, 2020', 'December 7, 2020', 'November 2, 2020', 'December 14, 2020',
                     'October 5, 2020', 'September 28, 2020', 'October 19, 2020', 'November 23, 2020', 'December 28, 2020',
                     'October 26, 2020', 'November 9, 2020', 'September 14, 2020', 'September 27, 2021', 'January 11, 2021',
                     'October 11, 2021', 'November 29, 2021', 'December 6, 2021', 'November 1, 2021', 'February 22, 2021',
                     'November 15, 2021', 'December 20, 2021', 'August 30, 2021', 'September 20, 2021', 'January 4, 2021',
                     'December 27, 2021', 'October 25, 2021', 'October 18, 2021', 'September 13, 2021', 'March 8, 2021',
                     'November 22, 2021', 'November 8, 2021', 'September 6, 2021', 'December 13, 2021', 'October 4, 2021']

    test_dir_lst = ['October 3, 2022', 'December 12, 2022', 'September 12, 2022',  'November 21, 2022', 'October 24, 2022',
                    'January 9, 2023', 'September 5, 2022', 'January 3, 2022', 'November 14, 2022', 'November 7, 2022',
                    'October 31, 2022', 'December 26, 2022', 'December 19, 2022',  'October 17, 2022', 'August 29, 2022',
                    'September 26, 2022', 'January 10, 2022', 'November 28, 2022', 'October 10, 2022',  'January 2, 2023',
                    'September 19, 2022',  'December 5, 2022']

    # for i in train_dir_lst:
        # if re.findall("^August.*2021", i):
        #     print(re.findall("^August.*2021", i))
        # if re.findall("^September.*2021", i):
        #     print(re.findall("^September.*2021", i))
        # if re.findall("^October.*2021", i):
        #     print(re.findall("^October.*2021", i))
        # if re.findall("^November.*2021", i):
        #     print(re.findall("^November.*2021", i))
        # if re.findall("^December.*2021", i):
        #     print(re.findall("^December.*2021", i))
        # if re.findall("^January.*2021", i):
        #     print(re.findall("^January.*2021", i))

    # merge games into weeks
    # for i in test_dir_lst:
    #     path = f"./{i}"
    #     files = glob.glob(path + "/*.csv")
    #     df_csv_concat = pd.concat([pd.read_csv(file) for file in reversed(files)])
    #     df_csv_concat.to_csv(f'./test_merge/{i}.csv')

    # merge weeks in to all_data
    # path = "./test_merge"
    # files = glob.glob(path + "/*.csv")
    # df_csv_concat = pd.concat([pd.read_csv(file) for file in reversed(files)])
    # df_csv_concat.to_csv(f'./test_merge/test_data.csv')

    # import module
    import pandas as pd

    # read the csv file
    test = pd.read_csv('test_merge/test_data.csv')
    train = pd.read_csv('train_merge/train_data.csv')
    res = len(test) + len(train)

    # display dataset
    print(res)
    all = pd.read_csv('merge_csv/all_data.csv')
    print(len(all))





